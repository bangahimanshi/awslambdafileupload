/**
 * @description      :
 * @author           : zoptal
 * @group            :
 * @created          : 14/10/2021 - 12:08:36
 *
 * MODIFICATION LOG
 * - Version         : 1.0.0
 * - Date            : 14/10/2021
 * - Author          : zoptal
 * - Modification    :
 **/
const AWS = require('aws-sdk')
const s3 = new AWS.S3({
  signatureVersion: 'v4',
  accessKeyId: 'AKIAVQJLN5IMVORDLTVP',
  secretAccessKey: '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',
  region: 'us-east-1'
})
const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path
const ffmpeg = require('fluent-ffmpeg')
ffmpeg.setFfmpegPath(ffmpegPath)
const { v4: uuidv4 } = require('uuid')

module.exports.getSignedUrl = async event => {
  try {
    const bucket = 'uploadl1'
    const key = uuidv4() + event.queryStringParameters.ext
    const expireSeconds = 60 * 5

    console.log(
      'evebnt---getSignedUrl------',
      JSON.stringify(event.queryStringParameters)
    )

    const url = await s3.getSignedUrl('putObject', {
      Bucket: bucket,
      Key: key,
      Expires: expireSeconds,
      ContentType: event.queryStringParameters.media
    })

    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      body: JSON.stringify({ url: url, key: key })
    }
  } catch (err) {
    console.log('rulr========', err)
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      body: JSON.stringify({ err })
    }
  }
}

module.exports.executePayload = async event => {
  try {
    const s3Event = event.Records[0].s3
    const params = {
      Bucket: s3Event.bucket.name,
      Key: s3Event.object.key
    }
    console.log('-executePayload params--->', event, '\n\n\n', params)
    s3.getObject(params, (err, data) => {
      if (err) {
        return err
      } else {
        console.log('I am here!')

        // callback(null, data.Body.toString('utf-8'))
        let result = JSON.parse(data.Body.toString())

        console.log(data)
        console.log(" data.Body.toString('utf-8')", data.Body.toString('utf-8'))
        return result
      }
    })

    /* Execute your business logic here. */
  } catch (err) {
    throw new Error(err)
  }
}

// module.exports.executePayload = async event => {
//   try {
//     const s3Event = event.Records[0].s3
//     const params = {
//       Bucket: s3Event.bucket.name,
//       Key: s3Event.object.key,
//       Expires: expireSeconds,
//       ACL: 'public-read'
//     }
//     console.log('-executePayload--->', JSON.stringify(event))
//     let data = await s3.getObject(params).promise()
//     let result = JSON.parse(data.Body.toString())
//     console.log('result', result)
//     // const objectData = data.Body.toString("utf-8");
//     // lines.push(objectData); // You might need to conversion here using JSON.parse(objectData);
//     // console.log(lines);
//     // return lines;
//     return {
//       statusCode: 200,
//       headers: {
//         'Access-Control-Allow-Origin': '*'
//       },
//       body: JSON.stringify(result)
//     }
//     /* Execute your business logic here. */
//   } catch (err) {
//     console.log('err', err)
//     throw new Error(err)
//   }
// }

// var AWS = require('aws-sdk');
// var request = require('request');
// var fs = require('fs');

// var params = {
//   Bucket: testData.Bucket,
//   Key: 'uploadtest.mov',
//   ContentType: 'video/quicktime'
// };
// return new AWS.S3().getSignedUrl('putObject', params, function (err, presigned_url) {
//   if (err) {
//     console.log('AWS.S3().getSignedUrl error' + err);
//   } else {
//     request({
//       method: 'PUT',
//       uri: presigned_url,
//       body: fs.readFileSync(testData.filePath),
//       headers: {
//         'Content-Type': 'video/quicktime'
//       }
//     },
//     function(error, response, body) {
//       if (error) {
//         console.error(error);
//       } else {
//         console.log('upload successful:', body);
//       }
//     });
//   }
// });
