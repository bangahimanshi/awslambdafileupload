/**
 * @description      :
 * @author           : zoptal
 * @group            :
 * @created          : 09/10/2021 - 12:31:48
 *
 * MODIFICATION LOG
 * - Version         : 1.0.0
 * - Date            : 09/10/2021
 * - Author          : zoptal
 * - Modification    :
 **/
/**
 *
 * @param {string} fileName the name in S3
 * @param {string} filePath the absolute path to our local file
 * @return the final file name in S3
 */
async function uploadToS3 (fileName, filePath, stream) {
  var fs = require('fs')
  const AWS = require('aws-sdk')
  AWS.config.update({
    accessKeyId: 'AKIAVQJLN5IMVORDLTVP',
    secretAccessKey: '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',
    region: 'us-east-1',
    max_concurrent_requests: 20,
    max_queue_size: 1000000,
    multipart_threshold: '100MB',
    multipart_chunksize: '64MB',
    max_bandwidth: '100MB/s',
    use_accelerate_endpoint: true
  }) 

  if (!fileName) {
    throw new Error('the fileName is empty')
  }
  // if (!filePath) {
  //   throw new Error('the file absolute path is empty')
  // }

  const fileNameInS3 = `${fileName}` // the relative path inside the bucket
  console.info(`file name: ${fileNameInS3} file path: ${filePath}`)

  // if (!fs.existsSync(filePath)) {
  //   throw new Error(`file does not exist: ${filePath}`)
  // }

  const bucket = 'testuploadpeachly'

  const s3 = new AWS.S3()

  // const statsFile = fs.statSync(filePath)
  // console.info(
  //   `\nfile size: ${Math.round(statsFile.size / 1024 / 1024)}MB`,
  //   statsFile
  // )

  //  Each part must be at least 5 MB in size, except the last part.
  let uploadId

  try {
    const params = {
      Bucket: bucket,
      Key: fileNameInS3,
      ACL: 'public-read'
    }
    const result = await s3.createMultipartUpload(params).promise()

    uploadId = result.UploadId

    console.info(
      `csv ${fileNameInS3} multipart created with upload id: ${uploadId}`
    )
  } catch (e) {
    throw new Error(`Error creating S3 multipart. ${e.message}`)
  }

  const chunkSize = 2 * 1024 * 1024 // 10MB
  const readStream = stream // you can use a second parameter here with this option to read with responsea bigger chunk size than 64 KB: { highWaterMark: chunkSize }

  // read the file to upload using streams and upload part by part to S3
  const uploadPartsPromise = new Promise((resolve, reject) => {
    const multipartMap = { Parts: [] }

    let partNumber = 1
    let chunkAccumulator = null

    readStream.on('error', err => {
      reject(err)
    })

    readStream.on('data', chunk => {
      // it reads in chunks of 64KB. We accumulate them up to 10MB and then we send to S3
      if (chunkAccumulator === null) {
        chunkAccumulator = chunk
      } else {
        chunkAccumulator = Buffer.concat([chunkAccumulator, chunk])
      }
      if (chunkAccumulator.length > chunkSize) {
        // pause the stream to upload this chunk to S3
        readStream.pause()

        const chunkMB = chunkAccumulator.length / 1024 / 1024

        const params = {
          Bucket: bucket,
          Key: fileNameInS3,
          PartNumber: partNumber,
          UploadId: uploadId,
          Body: chunkAccumulator,
          ContentLength: chunkAccumulator.length
        }
        s3.uploadPart(params)
          .promise()
          .then(result => {
            console.info(
              `Data uploaded. Entity tag: ${result.ETag} Part: ${params.PartNumber} Size: ${chunkMB}`
            )
            multipartMap.Parts.push({
              ETag: result.ETag,
              PartNumber: params.PartNumber
            })
            partNumber++
            chunkAccumulator = null
            // resume to read the next chunk
            readStream.resume()
          })
          .catch(err => {
            console.error(`error uploading the chunk to S3 ${err.message}`)
            reject(err)
          })
      }
    })

    readStream.on('end', () => {
      console.info('End of the stream')
    })

    readStream.on('close', () => {
      console.info('Close stream')
      if (chunkAccumulator) {
        const chunkMB = chunkAccumulator.length / 1024 / 1024

        // upload the last chunk
        const params = {
          // ACL: 'public-read',
          Bucket: bucket,
          Key: fileNameInS3,
          PartNumber: partNumber,
          UploadId: uploadId,
          Body: chunkAccumulator,
          ContentLength: chunkAccumulator.length
        }

        s3.uploadPart(params)
          .promise()
          .then(result => {
            console.info(
              `Last Data uploaded. Entity tag: ${result.ETag} Part: ${params.PartNumber} Size: ${chunkMB}`
            )
            multipartMap.Parts.push({
              ETag: result.ETag,
              PartNumber: params.PartNumber
            })
            chunkAccumulator = null
            resolve(multipartMap)
          })
          .catch(err => {
            console.error(
              `error uploading the last csv chunk to S3 ${err.message}`
            )
            reject(err)
          })
      }
    })
  })

  const multipartMap = await uploadPartsPromise

  console.info(
    `All parts have been upload. Let's complete the multipart upload. Parts: ${multipartMap.Parts.length} `
  )

  // gather all parts' tags and complete the upload
  try {
    const params = {
      Bucket: bucket,
      Key: fileNameInS3,
      MultipartUpload: multipartMap,
      UploadId: uploadId
    }
    var result = await s3.completeMultipartUpload(params).promise()
    console.info(
      `Upload multipart completed. Location: ${result.Location} Entity tag: ${result.ETag}`
    )
  } catch (e) {
    throw new Error(`Error completing S3 multipart. ${e.message}`)
  }
  return `Upload multipart completed. Location: ${result.Location} Entity tag: ${result.ETag}`
}

exports.handler = async event => {
  var multipart = require('parse-multipart')

  var bodyBuffer = Buffer.from(event['body-json'].toString(), 'base64')

  var boundary = multipart.getBoundary(event.params.header['content-type'])

  var parsedData = multipart.Parse(bodyBuffer, boundary)

  var stream = await bufferToStream(bodyBuffer)

  console.log('parsed data-->', parsedData[0].data, stream)
  let fnFileName =
    'fn_peachly' + Math.floor(Date.now() / 1000) + '_file_video.mp4'

  let finalResponse = await uploadToS3(
    parsedData[0].filename,
    'filePath',
    stream
  )
  // TODO implement

  const response = {
    statusCode: 200,
    body: finalResponse
  }
  return response
}

var Readable = require('stream').Readable

function bufferToStream (buffer) {
  var stream = new Readable()
  stream.push(buffer)
  stream.push(null)

  return stream
}
