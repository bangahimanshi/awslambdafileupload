/**
 * @description      :
 * @author           : zoptal
 * @group            :
 * @created          : 21/10/2021 - 16:49:30
 *
 * MODIFICATION LOG
 * - Version         : 1.0.0
 * - Date            : 21/10/2021
 * - Author          : zoptal
 * - Modification    :
 **/
var fs = require('fs')
// const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path
// const ffmpeg = require('fluent-ffmpeg')
// ffmpeg.setFfmpegPath(ffmpegPath)
// var writeStream = fs.createWriteStream('output.mp4')
// const AWS = require('aws-sdk')
// const s3 = new AWS.S3({
//   signatureVersion: 'v4',
//   accessKeyId: 'AKIAVQJLN5IMVORDLTVP',
//   secretAccessKey: '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',
//   region: 'us-east-1'
// })



const request = require('request')
const apiEndpoint =
  'https://zrn8pqlp1g.execute-api.us-east-1.amazonaws.com/dev/s3url?type=image'
const todosEndpoint =
  'https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_1280_10MG.mp4'

var fetch = todosEndpoint =>
  import('node-fetch').then(({ default: fetch }) => fetch(todosEndpoint))

upload = (async function () {
  const todos = await getTodos()

  const data = await getSignedUrl()
  console.log(todos)
  const result = await uploadToS3(data.url, todos)
  return result
})()

async function getTodos () {
  return [
    // fs.readFileSync('/home/zoptal/Videos/1L.avi')
    fs.readFileSync('/home/zoptal/Videos/file_example_MP4_1280_10MG.mp4')
  ]
}

async function getSignedUrl () {
  return fetch(apiEndpoint)
    .then(res => res.json())
    .then(data => data)
}

async function uploadToS3 (url, todos) {
  // params = {
  //   ACL: 'public-read',
  //   Bucket: 'peachlylamda',
  //   Key: '1L1.mp4', // File name you want to save as in S3
  //   Body: todos
  // }

  // // Uploading files to the bucket
  // s3.upload(params, function (err, data) {
  //   if (err) {
  //     throw err
  //   }
  //   console.log(`File uploaded successfully. ${data.Location}`)
  // })

  var params = {
    method: 'PUT',
    headers: {
      'Content-Type': 'video/quicktime'
    },
    body: todos
  }

  request(url, params, function (error, response, body) {
    if (error) {
      console.log('error :', response.statusCode)
      return error
    } else if (response && response.statusCode != 200) {
      return 'File is corrupted'
    } else {
      console.log(response.statusCode, 'Successfully Uploaded to S3')
      return `Successfully Uploaded to S3 `
    }
  })
}
